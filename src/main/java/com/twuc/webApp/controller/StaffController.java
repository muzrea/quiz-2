package com.twuc.webApp.controller;

import com.twuc.webApp.contract.StaffRequest;
import com.twuc.webApp.contract.ZoneIdRequest;
import com.twuc.webApp.model.Staff;
import com.twuc.webApp.model.StaffRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class StaffController {
    //TODO: field injection is not recommended
    @Autowired
    StaffRepository staffRepository;
    //TODO: field injection is not recommended
    //TODO: unused field
    @Autowired
    EntityManager em;

    @PostMapping("/staffs")
    public ResponseEntity postStaff(@RequestBody @Valid StaffRequest staffRequest) {
        Staff staff = staffRepository.save(new Staff(staffRequest.getFirstName(), staffRequest.getLastName(),staffRequest.getZoneId()));

        //TODO: 为啥需要flush？
        staffRepository.flush();
        return ResponseEntity.status(201)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .header("Location","/api/staffs/"+staff.getId())
                .body(staff);
    }

    @GetMapping("/staffs/{staffId}")
    public ResponseEntity postStaff(@PathVariable Long staffId) {
        Optional<Staff> staff = staffRepository.findById(staffId);
        staffRepository.flush();
        if (staff.isPresent()) {
            return ResponseEntity.status(200)
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .body(staff.get());
        }
        return ResponseEntity.status(404).build();
    }

    @GetMapping("/staffs")
    public ResponseEntity postStaff() {
        List<Staff> staffs = staffRepository.findAll();
        staffRepository.flush();
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(staffs);
    }

    @PutMapping("/staffs/{staffId}/timezone")
    public ResponseEntity putStaffZoneId(@PathVariable Long staffId,@RequestBody ZoneIdRequest zoneId) {
        Optional<Staff> staff = staffRepository.findById(staffId);
        staffRepository.flush();
        if(!staff.isPresent()){
            return ResponseEntity.notFound().build();
        }
        staff.get().setZoneId(zoneId.getZoneId());
        staffRepository.saveAndFlush(staff.get());
        return ResponseEntity.status(201).contentType(MediaType.APPLICATION_JSON_UTF8)
                .build();
    }
}
