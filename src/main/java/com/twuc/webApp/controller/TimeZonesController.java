package com.twuc.webApp.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.zone.ZoneRulesProvider;
import java.util.Arrays;

@RestController
@RequestMapping("/api")
public class TimeZonesController {

    @GetMapping("/timezones")
    public ResponseEntity getTimezones(){
        Object[] timezones = ZoneRulesProvider.getAvailableZoneIds().toArray();
        Arrays.sort(timezones);
        return ResponseEntity.status(200)
                .contentType(MediaType.APPLICATION_JSON_UTF8 )
                .body(timezones);
    }
}
