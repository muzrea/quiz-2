package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StaffRequest {
    @NotNull
    @Size(max = 64)
    private String firstName;
    @NotNull
    @Size(max = 64)
    private String lastName;
    private String zoneId;

    //TODO: 如果你用了TDD，或者YAGNI，这个constructor不会有annotation
    public StaffRequest(@NotNull @Size(max = 64) String firstName, @NotNull @Size(max = 64) String lastName,String zoneId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.zoneId = zoneId;
    }

    public StaffRequest() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
