package com.twuc.webApp.contract;

public class ZoneIdRequest {
    private String zoneId;

    public ZoneIdRequest(String zoneId) {
        this.zoneId = zoneId;
    }

    public ZoneIdRequest() {
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }
}
