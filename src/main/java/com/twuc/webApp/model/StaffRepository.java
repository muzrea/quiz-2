package com.twuc.webApp.model;

import org.springframework.data.jpa.repository.JpaRepository;

//TODO: 加上@Repository
public interface StaffRepository extends JpaRepository<Staff,Long> {
//    public Staff getStaffById(Long id);
}
