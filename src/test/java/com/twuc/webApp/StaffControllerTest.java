package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.StaffRequest;
import com.twuc.webApp.model.Staff;
import com.twuc.webApp.model.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import javax.persistence.EntityManager;

import java.time.zone.ZoneRulesProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.mock.http.server.reactive.MockServerHttpRequest.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class StaffControllerTest extends ApiTestBase {
    @Autowired
    EntityManager em;

    @Autowired
    StaffRepository staffRepository;

    @Test
    void should_save_staff() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new StaffRequest("Bob", "Micheal", "Asia/Chongqing"))))
                .andExpect(status().is(201))
                .andExpect(header().stringValues("/api/staffs/1"))
                .andExpect(content().json("{\"firstName\": \"Bob\",\"lastName\": \"Micheal\",\"zoneId\": \"Asia/Chongqing\"}"));
    }

    @Test
    void should_return_400_when_staff_is_invalid() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(
                        new StaffRequest("Bob", "MichealMichealMichealMichealMichealMichealMichealMichealMichealMichealMicheal", null))))
                .andExpect(status().is(400));
    }

    @Test
    void should_get_staff() throws Exception {
        Staff staff = new Staff("Bob", "Micheal", null);
        em.persist(staff);
        mockMvc.perform(get("/api/staffs/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(200))
                .andExpect(content().string(new ObjectMapper().writeValueAsString(staff)));
    }

    @Test
    void should_return_404_when_staff_does_not_exist() throws Exception {
        mockMvc.perform(get("/api/staffs/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(404));
    }

    @Test
    void should_find_all_staffs() throws Exception {
        Staff staff = new Staff("Bob", "Micheal", null);
        em.persist(staff);
        Staff staff2 = new Staff("Anna", "Micheal", null);
        em.persist(staff2);
        List<Staff> staffs = new ArrayList<>();
        staffs.add(staff);
        staffs.add(staff2);
        MvcResult mvcResult = mockMvc.perform(get("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
        assertEquals(200, mvcResult.getResponse().getStatus());
        // TODO: 2019-10-12
    }

    @Test
    void should_find_staff_timezone() throws Exception {
        Staff staff = new Staff("Bob", "Micheal", null);
        staffRepository.save(staff);
        staffRepository.flush();
        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.put("/api/staffs/1/timezone")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content("{ \"zoneId\": \"Asia/Chongqing\" }");
        this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .is(201));
    }

    @Test
    void should_save_staff_when_zone_id_is_null() throws Exception {
        mockMvc.perform(post("/api/staffs")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(new ObjectMapper().writeValueAsString(new StaffRequest("Bob", "Micheal", null))))
                .andExpect(status().is(201))
                .andExpect(header().stringValues("/api/staffs/1"))
                .andExpect(content().json("{\"firstName\": \"Bob\",\"lastName\": \"Micheal\",\"zoneId\": null}"));
    }

    @Test
    void should_get_timezones_asc() throws Exception {
        Set<String> availableZoneIds = ZoneRulesProvider.getAvailableZoneIds();
        List<String> zoneIds = new ArrayList<>(availableZoneIds);
        Collections.sort(zoneIds);
        mockMvc.perform(get("/api/timezones")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string(serialize(zoneIds)));
    }
}
